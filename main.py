import sqlite3
import tkinter as tk
from typing import Type
import tkcalendar as tkc
import tkinter.messagebox
from sqlfunctions import *
from Additional_GUI import *

class MainGui(tk.Frame):
    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        self.parent = parent
        
        try:
            mydb = sqlite3.connect("ergasia_omada_3.db") # connect to db
            print(mydb)
        except Exception as E:
            print(E)
            tkinter.messagebox.showerror("Database Error", E)
        
        self.init(mydb)

    #====INIT WINDOW=========================================================================
    def init(self, db : sqlite3.Connection):
        self.mydb = db
        self.cursor = self.mydb.cursor()
        execute_query(self.mydb, self.cursor, "PRAGMA foreign_keys = ON;") # Enable foreign keys, it's not enabled by default

        #---------------------------------------------
        menubar = tk.Menu(self.master)
        self.master.config(menu=menubar)

        #----------File MENU---------------------------------
        fileMenu = tk.Menu(menubar, tearoff=False)
        fileMenu.add_command(label="Custom query", command = lambda:self.CustomqueryGui())
        fileMenu.add_command(label="Exit",  command=self.onExit)
        menubar.add_cascade(label="File",   menu=fileMenu)

        #----------View MENU---------------------------------
        viewMenu = tk.Menu(menubar, tearoff=False)
        viewMenu.add_command(label="Property",          command=lambda:self.view("PROPERTY"))
        viewMenu.add_command(label="Locations",         command=lambda:self.view("LOCATION"))
        viewMenu.add_command(label="Owners",            command=lambda:self.view("OWNER"))
        viewMenu.add_command(label="Home",              command=lambda:self.view("HOME"))
        viewMenu.add_command(label="Business",          command=lambda:self.view("BUSINESS"))
        viewMenu.add_command(label="Land",              command=lambda:self.view("LAND"))
        viewMenu.add_command(label="Available Sale",    command=lambda:self.view_available("SALE"))
        viewMenu.add_command(label="Available Rental",  command=lambda:self.view_available("RENTAL"))
        viewMenu.add_command(label="Sale",              command=lambda:self.view("SALE"))
        viewMenu.add_command(label="Rental",            command=lambda:self.view("RENTAL"))
        viewMenu.add_command(label="Goes_For",          command=lambda:self.view("GOES_FOR"))
        menubar.add_cascade(label="View",               menu=viewMenu)

        #----------ADD MENU---------------------------------
        addMenu = tk.Menu(menubar, tearoff=False)
        addMenu.add_command(label="Property",           command=lambda:self.addEntryGui("PROPERTY"))
        addMenu.add_command(label="Locations",          command=lambda:self.addEntryGui("LOCATION"))
        addMenu.add_command(label="Owners",             command=lambda:self.addEntryGui("OWNER"))
        addMenu.add_command(label="Home",               command=lambda:self.addEntryGui("HOME"))
        addMenu.add_command(label="Business",           command=lambda:self.addEntryGui("BUSINESS"))
        addMenu.add_command(label="Land",               command=lambda:self.addEntryGui("LAND"))
        addMenu.add_command(label="Sale",               command=lambda:self.addEntryGui("SALE"))
        addMenu.add_command(label="Rental",             command=lambda:self.addEntryGui("RENTAL"))
        addMenu.add_command(label="Goes_For",           command=lambda:self.addEntryGui("GOES_FOR"))
        menubar.add_cascade(label="Add",                menu=addMenu)
         
        #-----Update Menu------------------------------------
        updateMenu = tk.Menu(menubar, tearoff=False)
        updateMenu.add_command(label="Property",        command=lambda:self.UpdateEntryGui("PROPERTY"))
        updateMenu.add_command(label="Locations",       command=lambda:self.UpdateEntryGui("LOCATION"))
        updateMenu.add_command(label="Owners",          command=lambda:self.UpdateEntryGui("OWNER"))
        updateMenu.add_command(label="Home",            command=lambda:self.UpdateEntryGui("HOME"))
        updateMenu.add_command(label="Business",        command=lambda:self.UpdateEntryGui("BUSINESS"))
        updateMenu.add_command(label="Land",            command=lambda:self.UpdateEntryGui("LAND"))
        updateMenu.add_command(label="Sale",            command=lambda:self.UpdateEntryGui("SALE"))
        updateMenu.add_command(label="Rental",          command=lambda:self.UpdateEntryGui("RENTAL"))
        updateMenu.add_command(label="Goes_For",        command=lambda:self.UpdateEntryGui("GOES_FOR"))
        menubar.add_cascade(label="Update",             menu=updateMenu)

        #-----REMOVE MENU--------------------------------------------
        removeMenu = tk.Menu(menubar, tearoff=False)
        removeMenu.add_command(label="Property",        command=lambda:self.removeEntryGui("PROPERTY"))
        removeMenu.add_command(label="Locations",       command=lambda:self.removeEntryGui("LOCATION"))
        removeMenu.add_command(label="Owners",          command=lambda:self.removeEntryGui("OWNER"))
        removeMenu.add_command(label="Home",            command=lambda:self.removeEntryGui("HOME"))
        removeMenu.add_command(label="Business",        command=lambda:self.removeEntryGui("BUSINESS"))
        removeMenu.add_command(label="Land",            command=lambda:self.removeEntryGui("LAND"))
        removeMenu.add_command(label="Sale",            command=lambda:self.removeEntryGui("SALE"))
        removeMenu.add_command(label="Rental",          command=lambda:self.removeEntryGui("RENTAL"))
        removeMenu.add_command(label="Goes_For",        command=lambda:self.removeEntryGui("GOES_FOR"))
        menubar.add_cascade(label="Remove",             menu=removeMenu)
        #------------------------------------------------------------

        self.widgets = {}
        self.table = None
        self.customqGui = None
        self.add = None
        self.updateGui = None
        self.removeGui = None
        self.view("PROPERTY")

    #== Custom query =======================================================================
    #========================================================================================
    def CustomqueryGui(self):
        try:
            self.customqGui.destroy()
        except:
            pass
        self.customqGui = tk.Tk()
        ExecuteCustomqueryGui(self.customqGui, self.cursor, self).pack(fill="both", expand=True)
        self.customqGui.resizable(True, True)
        self.customqGui.mainloop()

    #== ADD ENTRY============================================================================
    #========================================================================================
    def addEntry(self, addlist, tb):
        r = False
        r = create_entry(self.mydb, self.cursor, tb, addlist)       

        if(r):
            self.view(tb)
            self.killEntryGui()

    def addEntryGui(self, var):
        try:
            self.add.destroy()
        except:
            pass
        self.add = tk.Tk()
        AddGui(self.add, self.cursor, var, self).pack(fill="both", expand=True)
        self.add.resizable(False,False)
        self.add.mainloop()

    def killEntryGui(self):
        self.add.destroy()
    
    #== REMOVE ENTRY=========================================================================
    #========================================================================================
    def removeEntry(self, tablename, itemID, item):
        r = deleteItems(self.mydb, self.cursor, tablename, itemID, item)
        if r == False:
            tkinter.messagebox.showerror("Delete Error", item + " doesnt exist in database")
        else:
            self.killRemoveEntryGui()
        self.view(tablename)
    
    def removeEntryGui(self, var):
        try:
            self.killRemoveEntryGui()
        except:
            pass
        self.removeGui = tk.Tk()
        RemoveGui(self.removeGui, self.cursor, var, self).pack(fill="both", expand=True)
        self.removeGui.resizable(False,False)
        self.removeGui.mainloop()
    
    def killRemoveEntryGui(self):
        if(self.removeGui != None):
            self.removeGui.destroy()
            self.removeGui = None

    #== UPDATE ENTRY=========================================================================
    #========================================================================================
    def updateEntry(self, tablename, itemID, pk, ls):
        r = update_entry(self.mydb, self.cursor, tablename, ls, id=itemID, pk=pk)
        if r == False:
            pass
        else:
            self.killRemoveEntryGui()
        self.view(tablename)

    def UpdateEntryGui(self, var):
        try:
            self.killUpdateEntryGui()
        except:
            pass
        self.updateGui = tk.Tk()
        EditGui(self.updateGui, self.cursor, var, self).pack(fill="both", expand=True)
        self.updateGui.resizable(False,False)
        self.updateGui.mainloop()

    def killUpdateEntryGui(self):
            self.updateGui.destroy()
        
    #== VIEW GRID============================================================================
    #========================================================================================
    def view(self, var):
        self.tablename = var
        data = get_table(self.cursor, var)  # Get Table Data
        self.view_table(data, var)

    def view_available(self, var):
        self.tablename = var
        data = get_table(self.cursor, var)
        temp = []
        for i in data:
            if i[-1] == None: # date is the last entry so if it not null the propery is sold/rented
                temp.append(i)

        data = temp
        self.table_fk = get_table_foreign_keys(self.cursor, var)
        
        self.land = get_table(self.cursor, "LAND")
        self.business = get_table(self.cursor, "BUSINESS")
        self.home = get_table(self.cursor, "HOME")

        self.create_grid(data, [tuple(get_table_column_names(self.cursor, var))])

    def view_id(self, var, pk, id):
        data = get_table_id(self.cursor, var, pk, id)
        self.view_table(data, var)

    def view_table(self, data, var):
        self.tablename = var
        self.table_fk = get_table_foreign_keys(self.cursor, var) # Get Foreign keys
        
        # if table == owner or location add custom fk 
        if var == "OWNER":
            tableinfo = get_primary_key(self.cursor, "OWNER")
            self.table_fk[tableinfo[0]] = {}
            self.table_fk[tableinfo[0]]["from"] = tableinfo[0]
            self.table_fk[tableinfo[0]]["table"] = "PROPERTY"
            self.table_fk[tableinfo[0]]["to"] = "owner_afm"
        elif var == "LOCATION":
            tableinfo = get_primary_key(self.cursor, "LOCATION")
            self.table_fk[tableinfo[0]] = {}
            self.table_fk[tableinfo[0]]["from"] = tableinfo[0]
            self.table_fk[tableinfo[0]]["table"] = "PROPERTY"
            self.table_fk[tableinfo[0]]["to"] = "area_code"

        self.land = get_table(self.cursor, "LAND")
        self.business = get_table(self.cursor, "BUSINESS")
        self.home = get_table(self.cursor, "HOME")
        
        self.create_grid(data, [tuple(get_table_column_names(self.cursor, var))])

    def search_in_tables(self, id):
        if self.search_in_table(self.land, id):
            return "LAND"
        if self.search_in_table(self.business, id):
            return "BUSINESS"
        if self.search_in_table(self.home, id):
            return "HOME"

    def search_in_table(self, tabledata, id):
        for i in tabledata:
            if i[0] == id:
                return True
        return False

    def remove_table_from_view(self): # Remove All items from grid
        if(len(self.widgets) != 0):
            for row in self.widgets:
                for i in self.widgets[row]:
                    self.widgets[row][i].destroy()
        try:
            self.table.destroy()
        except:
            pass
        try:
            self.head.destroy()
        except:
            pass
          
    def create_grid(self, data, indx):
        self.remove_table_from_view()
        
        self.widgets = {}
        self.widgets[0] = {}

        # Column names 
        columnnum = 0
        indx = indx[0]
        
        self.head = tk.Frame(self, height=15)
        self.head.grid(row=0, column=0, sticky="ew")
        
        for item in indx:
            self.widgets[0][item] = tk.Label(self.head, text=item if item != None else "Null", borderwidth=5, relief="solid", width=20)
            self.widgets[0][item].grid(row=0, column=columnnum, sticky="nsew")
            self.head.grid_columnconfigure(columnnum, weight=1) # For Resizing
            columnnum += 1
        #=============================================================================================================

        # Add a canvas in that frame
        self.table = tk.Frame(self)
        self.table.grid(row=1, column=0, sticky="nsew")

        self.grid_rowconfigure(1, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.canvas = tk.Canvas(self.table)
        self.canvas.pack(side = tk.LEFT, fill=tk.BOTH, expand=True)

        # ScrollBar        
        vsb = tk.Scrollbar(self, orient="vertical", command=self.canvas.yview)
        vsb.grid(row=0, column=1, rowspan=2, sticky="ns")
        self.canvas.configure(yscrollcommand=vsb.set)

        # Create a frame to contain the labels
        self.frame_Entry = tk.Frame(self.canvas, bg="blue")
        self.canvas.create_window((0, 0), window=self.frame_Entry, anchor='nw', tags="self.frame_Entry")
        
        row = 0
        for rowd in (data): # Data grid
            row += 1
            self.widgets[row] = {}
            columnnum = 0
            for item in range(len(rowd)):
                if(indx[item] in self.table_fk): # If it is foreign key, we put a Button instead of label
                    self.widgets[row][indx[item]] = tk.Button(self.frame_Entry, text = rowd[item] if rowd[item] != None else "Null", borderwidth=1, relief="solid", width=20, command = lambda
                        var = self.table_fk[indx[item]]['table'],
                        row_name = self.table_fk[indx[item]]['to'],
                        item_id = rowd[item] : self.view_id(var, row_name, item_id) )
                
                elif self.tablename == "PROPERTY" and indx[item] == get_primary_key(self.cursor, self.tablename)[0]:    # If it is property PRIMARY key 
                    try: # we get type Error if item not found in home/land/business
                        self.widgets[row][indx[item]] = tk.Button(self.frame_Entry, text=rowd[item] if rowd[item] != None else "Null", borderwidth=1, relief="solid", width=20, command = lambda
                        table = self.search_in_tables(rowd[0]), 
                        pk = get_primary_key(self.cursor, self.search_in_tables(rowd[0]))[0], row = rowd[0]: self.view_id(table, pk, row))
                    except TypeError as e:
                        self.widgets[row][indx[item]] = tk.Label(self.frame_Entry, text=rowd[item] if rowd[item] != None else "Null", borderwidth=1, relief="solid", width=20)    

                else:
                    self.widgets[row][indx[item]] = tk.Label(self.frame_Entry, text=rowd[item] if rowd[item] != None else "Null", borderwidth=1, relief="solid", width=20)
                
                self.widgets[row][indx[item]].grid(row=row, column=columnnum, sticky="nsew")
                
                self.frame_Entry.grid_columnconfigure(columnnum, weight=1) # For Resizing
                columnnum += 1

        self.frame_Entry.grid_rowconfigure(row+1, weight=1)

        self.frame_Entry.update_idletasks()
        
        # Set the canvas scrolling region
        self.canvas.config(scrollregion=self.canvas.bbox("all"), yscrollincrement=5)
        self.canvas.bind("<Configure>", self.onCanvasConfig)
        self.canvas.bind_all("<MouseWheel>", lambda event: self._on_mousewheel(event))
        self.canvas.bind_all("<Button-4>",   lambda event: self._on_mousewheel(event))
        self.canvas.bind_all("<Button-5>",   lambda event: self._on_mousewheel(event))
        
        # Set Window Size
        self.minwidth = self.frame_Entry.winfo_width() + vsb.winfo_width() + 10
        self.parent.geometry(f'{int(self.minwidth/1.5)}x200')
        self.parent.title(self.tablename)

    def _on_mousewheel(self, event):
        def delta(event): # linux support
            if event.num == 4 or event.delta > 0:
                return -1 
            return 1 

        self.canvas.yview_scroll(delta(event), "units")

    def onCanvasConfig(self, event): # Configure Canvas
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))
        self.canvas.itemconfig("self.frame_Entry", width=self.canvas.winfo_width())
    #========================================================================================
    #========================================================================================
    def onExit(self):
        self.quit()


if __name__ == "__main__":
    root = tk.Tk()
    MainGui(root).pack(fill="both", expand=True)
    root.mainloop()



#                                                                 000000                 00
#                                                                00000000                 00
#                                                               0000000000                 000
#                                                              00000  00000                 000
#                                                             00000    00000                 000
#                                                            00000      00000                 000
#                                                           00000        00000                0000
#                                                          00000          00000                0000
#     000000                                              00000            00000               0000
#    00000000                                            00000              00000              00000
#    00000000                                           00000                00000              00000
#     000000                                           00000                  00000             00000
#                                                     00000                    00000             00000
#                                                                                                00000
#                                                                                                00000
#                                                                                                00000
#                                                                                                00000
#                                                                                                00000
#     000000                                                                                     00000
#    00000000                                                                                   00000
#    00000000                                                                                   0000
#     000000                                                                                    0000
#                                                                                              0000
#                                                                                             0000   
#                                                                                             0000   
#                                                                                            0000    
#                                                                                            000    
#                                                                                           000 
#                                                                                          000  
#                                                                                         000
#                                                                                        00
#                                                                                       00