Ομάδα 3:  Εφαρμογή αναζήτησης ακινήτου (π.χ., κατοικία, επαγγελματικός χώρος, γη) για αγορά/ενοικίαση


Install Python Dependencies:

Windows:    ```pip install tkcalendar```

Linux:  ```pip3 install tkcalendar```


Convert ergasia_omada_3.sql file to ergasia_omada_3.db

Windows:
Use DB browser to convert because commandline tool does not like Greek Letters


Linux: 
```cat ergasia_omada_3.sql | sqlite3 ergasia_omada_3.db```


To Use the function File->Custom Query, you must run it with terminal/command prompt Because it's printed to it
