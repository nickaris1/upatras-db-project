import sqlite3
import tkinter as tk
import tkcalendar as tkc
import tkinter.messagebox
import time
from sqlfunctions import *

class AddGui(tk.Frame):
    def __init__(self, parent, cur,  tm, parentW):
        parent.title("Add " + tm)
        tk.Frame.__init__(self, parent)
        
        self.cursor = cur
        self.tablename = tm
        self.parentWindow = parentW
        #---------------------------------------------
        col = get_table_column_names(self.cursor, self.tablename)
        self.tableinfo = get_table_info(self.cursor, self.tablename)

        self.tabledata = get_table(self.cursor, self.tablename)[-1][0] #last entry id
        
        #--------------------------------------------
        self.widgets = {}
        self.table = tk.Frame(self)
        self.table.pack(side="top", fill="both", expand=True)
        # self.v = 
        row = 0
        for i in col:   # Create entries
            row += 1
            self.widgets[i] = {}
            
            self.widgets[i][0] = tk.Label(self.table, text=i,anchor=tk.W)
            self.widgets[i][0].grid(row=row, column=0, sticky="nsew")
            try:  
                if ("date" in self.tableinfo[i]["type"].lower()): 
                    self.widgets[i][1] = {}
                    self.widgets[i][1][0] = tk.Frame(self.table)
                    self.widgets[i][1][0].grid(row=row, column=1)
                
                    self.widgets[i][1][1] = tkc.DateEntry(self.widgets[i][1][0], width=12, background='darkblue', foreground='white', borderwidth=2)
                    self.widgets[i][1][1].pack()
                
                elif (self.tableinfo[i]["type"].lower() in "boolean"):
                    self.widgets[i][1] = {}
                    dvalue = self.tableinfo[i]["default"]
                    dvalue = convert_to_type("bool", dvalue if dvalue != None else False)
    
                    self.widgets[i][2] = tk.BooleanVar() # variable for radiobuttons
                    self.widgets[i][1][0] = tk.Frame(self.table)
                    self.widgets[i][1][0].grid(row=row, column=1)
                    self.widgets[i][1][1] = tk.Radiobutton(self.widgets[i][1][0], text="True" , padx=20, variable = self.widgets[i][2], value=True)
                    self.widgets[i][1][2] = tk.Radiobutton(self.widgets[i][1][0], text="False", padx=20, variable = self.widgets[i][2], value=False)
                    self.widgets[i][1][1].pack(side=tk.LEFT)
                    self.widgets[i][1][2].pack(side=tk.RIGHT)
                    self.widgets[i][2].set(dvalue)
                else:
                    self.widgets[i][1] = {}
                    self.widgets[i][1][1] = tk.Entry(self.table)
                    if(self.tableinfo[i]["pk"] == 1):
                        self.widgets[i][1][1].insert(0, str(int(self.tabledata) + 1))
                    self.widgets[i][1][1].grid(row=row, column=1)
            except Exception as E:
                print(E)
          
        row += 1
        bn = tk.Button(self, text="ADD", command = self.finish)
        bne = tk.Button(self, text="Exit", command = self.onExitd)
        bn.pack()
        bne.pack()

        self.table.grid_columnconfigure(1, weight=1)
        self.table.grid_columnconfigure(2, weight=1)
        # invisible row after last row gets all extra space
        self.table.grid_rowconfigure(row+1, weight=1)

    def finish(self):
        addlist = []
        for i in self.widgets:
            if("date" in self.tableinfo[i]['type'].lower()):
                addlist.append(self.widgets[i][1][1].get_date())

            elif("bool" in self.tableinfo[i]['type'].lower()):
                addlist.append(str(self.widgets[i][2].get()))

            else:
                value = self.widgets[i][1][1].get()
                if(len(value) == 0):
                    if(self.tableinfo[i]["default"] != None):
                        addlist.append(self.tableinfo[i]["default"])

                    else:
                        if(not self.tableinfo[i]["notnull"]):
                            addlist.append("")
                        else:
                            tkinter.messagebox.showerror("Error", f"{i} Entry can not be NULL")
                else:
                    addlist.append(value)
            
        self.parentWindow.addEntry(addlist, self.tablename)

    def onExitd(self):
        self.parentWindow.killEntryGui()

class RemoveGui(tk.Frame):
    def __init__(self, parent, cur,  tm, parentW):
        parent.title("Remove " + tm)
        tk.Frame.__init__(self, parent)
        self.cursor = cur
        self.tablename = tm
        self.parent = parent
        self.parentWindow = parentW
        #---------------------------------------------
        self.col = get_table_column_names(self.cursor, self.tablename) # column name

        self.table = tk.Frame(self)
        self.table.pack(side="top", fill="both", expand=True)
        self.label = tk.Label(self.table, text=self.col[0], anchor=tk.W) # first column name
        self.entry = tk.Entry(self.table)
        self.label.grid(row=0,column=0, sticky="nsew")
        self.entry.grid(row=0,column=1, sticky="nsew")

        self.bn = tk.Button(self.table, text="Remove", command=self.delete)
        self.bn.grid(row=1, column=0, columnspan = 2,sticky="nsew")
            
        self.parent.bind('<Return>', self.enterConBind)

    def enterConBind(self, event):
        self.delete()

    def delete(self):
        self.parentWindow.removeEntry(self.tablename, self.col[0], self.entry.get()) # first column name

class ExecuteCustomqueryGui(tk.Frame):
    def __init__(self, parent, cur, parentW):
        parent.title("Custom sql ")
        tk.Frame.__init__(self, parent)
        self.mdb = parentW.mydb
        self.cursor : sqlite3.Cursor = cur
        self.parent = parent
        self.parentWindow = parentW
        #---------------------------------------------
        self.table = tk.Frame(self)
        self.table.pack(side="top", fill="both", expand=True)
        self.label = tk.Label(self.table, text="query:", anchor=tk.W)
        self.entry = tk.Entry(self.table)
        self.label.grid(row=0,column=0, sticky="nsw")
        self.entry.grid(row=0,column=1, sticky="nsew")

        self.bn = tk.Button(self.table, text="Execute", command=self.Execute)
        self.bn.grid(row=1, column=0, columnspan = 2,sticky="nsew")

        self.table.grid_rowconfigure(0, weight=1)
        self.table.grid_columnconfigure(0, weight=1)
        self.table.grid_rowconfigure(1, weight=1)
        self.table.grid_columnconfigure(1, weight=3)

        self.parent.bind('<Return>', lambda event : self.Execute(event))

    def Execute(self, e = None):   # Excecute query
        try:
            q = self.entry.get()
            if q[-1] != ";":
                q += ";"
            execute_query(self.mdb, self.cursor, f"{q}")
            for i in self.cursor.fetchall():
                print(i)
        except sqlite3.Warning as E:
            print(E)
       
class EditGui(tk.Frame):
    def __init__(self, parent, cur,  tm, parentW):
        parent.title("Update " + tm)
        tk.Frame.__init__(self, parent)
        self.cursor = cur
        self.tablename = tm
        self.parent = parent
        self.parentWindow = parentW
        #---------------------------------------------
        self.col = get_table_column_names(self.cursor, self.tablename)

        self.table = tk.Frame(self)
        self.table.pack(side="top", fill="both", expand=True)
        self.label = tk.Label(self.table, text=self.col[0], anchor=tk.W)
        self.entry = tk.Entry(self.table)
        self.label.grid(row=0,column=0, sticky="nsew")
        self.entry.grid(row=0,column=1, sticky="nsew")

        self.bn = tk.Button(self.table, text="Select", command=self.select)
        self.bn.grid(row=1, column=0, columnspan = 2,sticky="nsew")
            
        self.parent.bind('<Return>', self.enterConBind)

    def enterConBind(self, event):
        self.select()

    def select(self):
        self.pk = get_primary_key(self.cursor, self.tablename)
        self.id = self.entry.get()
        data = get_table_id(self.cursor, self.tablename, self.pk[0], self.id)[0]
        col = get_table_column_names(self.cursor, self.tablename)
        self.tableinfo = get_table_info(self.cursor, self.tablename)

        self.table.destroy()
        self.label.destroy()
        self.entry.destroy()
        
        #--------------------------------------------
        self.widgets = {}
        self.table = tk.Frame(self)
        self.table.pack(side="top", fill="both", expand=True)
        row = 0
        for i in range(len(col)):
            row += 1
            self.widgets[col[i]] = {}
            self.widgets[col[i]][0] = tk.Label(self.table, text=col[i],anchor=tk.W)
            self.widgets[col[i]][0].grid(row=row, column=0, sticky="nsew")
            try:  
                if ("date" in self.tableinfo[col[i]]["type"].lower()): 
                    self.widgets[col[i]][1] = {}
                    self.widgets[col[i]][1][0] = tk.Frame(self.table)
                    self.widgets[col[i]][1][0].grid(row=row, column=1)
                
                    self.widgets[col[i]][1][1] = tkc.DateEntry(self.widgets[col[i]][1][0], width=12, background='darkblue', foreground='white', borderwidth=2)
                    self.widgets[col[i]][1][1].set_date(data[i])
                    self.widgets[col[i]][1][1].pack()
                
                elif (self.tableinfo[col[i]]["type"].lower() in "boolean"):
                    self.widgets[col[i]][1] = {}

                    self.widgets[col[i]][2] = tk.BooleanVar() # variable for radiobuttons
                    self.widgets[col[i]][1][0] = tk.Frame(self.table)
                    self.widgets[col[i]][1][0].grid(row=row, column=1)
                    self.widgets[col[i]][1][1] = tk.Radiobutton(self.widgets[col[i]][1][0], text="True" , padx=20, variable = self.widgets[col[i]][2], value=True)
                    self.widgets[col[i]][1][2] = tk.Radiobutton(self.widgets[col[i]][1][0], text="False", padx=20, variable = self.widgets[col[i]][2], value=False)
                    self.widgets[col[i]][1][1].pack(side=tk.LEFT)
                    self.widgets[col[i]][1][2].pack(side=tk.RIGHT)
                    self.widgets[col[i]][2].set(data[i])
                else:
                    self.widgets[col[i]][1] = {}
                    self.widgets[col[i]][1][1] = tk.Entry(self.table)
                    self.widgets[col[i]][1][1].insert(tk.END, data[i])
                    
                    self.widgets[col[i]][1][1].grid(row=row, column=1)
            except Exception as E:
                print(E)
          
        row += 1
        bn = tk.Button(self, text="Edit", command = self.finish)
        bne = tk.Button(self, text="Exit", command = self.onExitd)
        self.parent.bind('<Return>', self.finish)
        bn.pack()
        bne.pack()

        self.table.grid_columnconfigure(1, weight=1)
        self.table.grid_columnconfigure(2, weight=1)
        # invisible row after last row gets all extra space
        self.table.grid_rowconfigure(row+1, weight=1)

    def finish(self):
        addlist = []
        for i in self.widgets:
            if("date" in self.tableinfo[i]['type'].lower()):
                addlist.append(self.widgets[i][1][1].get_date())

            elif("bool" in self.tableinfo[i]['type'].lower()):
                addlist.append(str(self.widgets[i][2].get()))

            else:
                value = self.widgets[i][1][1].get()
                if(len(value) == 0):
                    if(self.tableinfo[i]["default"] != None):
                        addlist.append(self.tableinfo[i]["default"])

                    else:
                        if(not self.tableinfo[i]["notnull"]):
                            addlist.append("")
                        else:
                            tkinter.messagebox.showerror("Error", f"{i} Entry can not be NULL")
                else:
                    addlist.append(value)
        self.parentWindow.updateEntry(self.tablename, self.id, self.pk[0], addlist)

    def onExitd(self):
        self.parentWindow.killUpdateEntryGui()
