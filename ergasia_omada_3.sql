PRAGMA foreign_keys = ON; 
PRAGMA encoding="UTF-8";
BEGIN TRANSACTION;
DROP TABLE IF EXISTS "PROPERTY";
CREATE TABLE IF NOT EXISTS "PROPERTY" (
    "id" integer				NOT NULL,
    "listed_price" integer,
    "tm" integer,
    "Road" varchar(255),
    "Address_num" integer,
    "Floor" integer,
    "Availability" boolean	DEFAULT FALSE	NOT NULL,
    "owner_afm" integer						NOT NULL,
    "area_code" integer,
    CONSTRAINT "PROPERTY_OWNER_FK" FOREIGN KEY("owner_afm") REFERENCES "OWNER"("AFM")				ON DELETE CASCADE		ON UPDATE CASCADE,
    CONSTRAINT "PROPERTY_LOCATION_FK" FOREIGN KEY("area_code") REFERENCES "LOCATION"("Area_code")	ON DELETE SET NULL		ON UPDATE CASCADE,
    PRIMARY KEY("id")
);

DROP TABLE IF EXISTS "LOCATION";
CREATE TABLE IF NOT EXISTS "LOCATION" (
    "Area_code" integer				NOT NULL,
    "City" varchar(255)				NOT NULL,
    "Area" varchar(255)				NOT NULL,
    "County" varchar(255)			NOT NULL,
    PRIMARY KEY("Area_code")
);

DROP TABLE IF EXISTS "OWNER";
CREATE TABLE IF NOT EXISTS "OWNER" (
    "AFM" integer				NOT NULL,
    "Fname" varchar(255)		NOT NULL,
    "Lname" varchar(255)		NOT NULL,
    "Phone" integer		NOT NULL,
    CONSTRAINT "PhoneLength"  CHECK("Phone" >= 0000000000 AND "Phone" < 10000000000),
    PRIMARY KEY("AFM")
);

DROP TABLE IF EXISTS "HOME";
CREATE TABLE IF NOT EXISTS "HOME" (
    "id" integer			NOT NULL,
    "type" varchar(255),
    "build_year" integer,
    "bedrooms" integer,
    "bathrooms" integer,
    "description" text,
    CONSTRAINT "HOME_PROPERTY_FK" FOREIGN KEY("id") REFERENCES "PROPERTY"("id")	ON UPDATE CASCADE	ON DELETE CASCADE,
    PRIMARY KEY("id")
);

DROP TABLE IF EXISTS "BUSINESS";
CREATE TABLE IF NOT EXISTS "BUSINESS" (
    "id" integer,
    "type" varchar(255),
    "build_year" integer,
    "description" text,
    CONSTRAINT "BUSINESS_PROPERTY_FK" FOREIGN KEY("id") REFERENCES "PROPERTY"("id") ON UPDATE CASCADE	ON DELETE CASCADE,
    PRIMARY KEY("id")
);

DROP TABLE IF EXISTS "LAND";
CREATE TABLE IF NOT EXISTS "LAND" (
    "id" integer	NOT NULL,
    "description" text,
    CONSTRAINT "LAND_PROPERTY_FK" FOREIGN KEY("id") REFERENCES "PROPERTY"("id") ON UPDATE CASCADE		ON DELETE CASCADE,
    PRIMARY KEY("id")
);

DROP TABLE IF EXISTS "SALE";
CREATE TABLE IF NOT EXISTS "SALE" (
    "sale_id" integer		NOT NULL,
    "price" real,
    "tm" real,
    "price_tm" real,
    "date" date,
    CONSTRAINT "SALE_GOES_FOR_FK" FOREIGN KEY("sale_id") REFERENCES "GOES_FOR"("id")	ON DELETE CASCADE		ON UPDATE CASCADE,
    PRIMARY KEY("sale_id")
);

DROP TABLE IF EXISTS "RENTAL";
CREATE TABLE IF NOT EXISTS "RENTAL" (
    "rental_id" integer		NOT NULL,
    "price_month" real,
    "tm" real,
    "price_tm" real,
    "warranty" integer,
    "startdate" date,
    "enddate" date,
    CONSTRAINT "RENTAL_GOES_FOR_FK" FOREIGN KEY("rental_id") REFERENCES "GOES_FOR"("id")	ON DELETE CASCADE	ON UPDATE CASCADE,
    PRIMARY KEY("rental_id")
);

DROP TABLE IF EXISTS "GOES_FOR";
CREATE TABLE IF NOT EXISTS "GOES_FOR" (
    "id" integer	NOT NULL,
    "property_id" integer,
    CONSTRAINT "GOES_FOR_PROPERTY_FK" FOREIGN KEY("property_id") REFERENCES "PROPERTY"("id") ON DELETE CASCADE	ON UPDATE CASCADE,
    PRIMARY KEY("id")

);



INSERT INTO "LOCATION" ("Area_code", "City", "Area", "County") VALUES
    (15125, "Αθήνα", 		"ΜΑΡΟΥΣΙ",      "Ελλάδα"),
    (15127, "Αθήνα", 		"ΜΕΛΙΣΣΙΑ",     "Ελλάδα"),
    (15451, "Αθήνα", 		"Ν.ΨΥΧΙΚΟ",     "Ελλάδα"),
    (17561, "Αθήνα", 		"Π.ΦΑΛΗΡΟ",     "Ελλάδα"),
    (17562, "Αθήνα", 		"Π.ΦΑΛΗΡΟ",     "Ελλάδα"),
    (26224, "Πάτρα", 		"ΨΗΛΑ ΑΛΩΝΙΑ",  "Ελλάδα"),
    (26442, "Πάτρα", 		"ΑΓ. ΣΟΦΙΑ",    "Ελλάδα"),
    (26221, "Πάτρα", 		"ΠΑΤΡΑ",	    "Ελλάδα"),
    (41222, "Λάρισα",		"ΠΑΠΑΣΤΑΥΡΟΥ",  "Ελλάδα"),
    (54351, "Θεσσαλονίκη", 	"ΑΝΩ ΤΟΥΜΠΑ", 	"Ελλάδα"),
    (55534, "Θεσσαλονίκη", 	"ΠΥΛΑΙΑ", 		"Ελλάδα");


INSERT INTO "OWNER" ("AFM", "Fname", "Lname", "Phone") VALUES
    (0000, "Sinus", 		"Lebastian", 	4200617621),
    (0001, "Pranav", 		"Farley", 		7830228455),
    (0002, "Anisha", 		"Macdonald", 	7330515533),
    (0003, "Elisabeth", 	"Haas",		 	0170314819),
    (0004, "Teresa", 		"Hurst", 		8630688413),
    (0005, "Roksana", 		"Burnett", 		2490426926),
    (0006, "Eboni", 		"Lees", 		7360915355),
    (0007, "Ella-Grace",	"Hebert", 		0060303109),
    (0008, "Ammaarah", 		"Jordan", 		9460411113),
    (0009, "Luna", 			"Amos", 		4150276408);



INSERT INTO "PROPERTY" ("id", "listed_price", "tm", "Road", "Address_num", "Floor", "Availability", "owner_afm", "area_code") VALUES
    (000000, 125000,	104,	"ΜΑΡΚΟΥ ΜΠΟΤΣΑΡΗ",	   	23,		5,	TRUE, 0008, 17562), 	--sale apt
    (000001, 103000,	81,		"ΑΝΔΡΟΥ",				 7,		2,	TRUE, 0004, 54351), 	--sale apt
    (000002, 5000,		550,	"ΟΛΥΜΠΟΥ", 			   	15,	 NULL,	TRUE, 0004, 55534), 	--rent villa
    (000003, 380,		32,		"ΚΑΝΑΡΗ", 				 2,		0,	TRUE, 0000, 41222), 	--rent studio
    (000004, 117000,	467,	"ΑΓΙΩΝ ΠΑΝΤΩΝ", 	   	12,  NULL,	TRUE, 0003, 26224), 	--sale land
    (000005, 35000,		139,	"ΚΩΝΣΤΑΝΤΙΝΟΥΠΟΛΕΩΣ", 	 7,	 NULL,	TRUE, 0001, 15125), 	--sale land
    (000006, 500,		62,		"ΜΙΜΟΖΑΣ", 			   	38,		4,	TRUE, 0008, 26442), 	--rent office
    (000007, 730,		90,		"ΔΙΑΓΟΡΑ", 			     6,		2,	TRUE, 0009, 15127), 	--rent apt
    (000008, 800,		280,	"ΑΓΙΟΥ ΔΗΜΗΤΡΙΟΥ", 	   	11,		0,	TRUE, 0002, 15451), 	--rent store
    (000009, 230000,	275,	"ΜΙΛΤΙΑΔΟΥ", 			21,  NULL,	TRUE, 0003, 41222), 	--sale house
    (000010, 2000,		350,	"ΑΜΕΡΙΚΗΣ",				 1,		1,	TRUE, 0007, 41222), 	--rent office
    (000011, 32000,		200,	"ΑΧΑΡΝΩΝ", 			   132,	 NULL,	TRUE, 0009, 17561), 	--sale land
    (000012, 100000,	60,		"ΑΓΙΑΣ ΕΙΡΗΝΗΣ", 		 6,		2,	TRUE, 0007, 41222), 	--sale office
    (000013, 10000000,	4600,	"ΚΟΛΟΚΟΤΡΩΝΗ", 			49,	 NULL,	TRUE, 0005, 26221), 	--sale hotel
    (000014, 450000,	130,	"ΠΟΝΤΟΥ", 				12,		3,	TRUE, 0001, 15127); 	--sale apt

INSERT INTO "GOES_FOR" ("id", "property_id") VALUES
(1000000, 000000),
(1000001, 000001),
(2000000, 000002),
(2000001, 000003),
(1000002, 000004),
(1000003, 000005),
(2000002, 000006),
(2000003, 000007),
(2000004, 000008),
(1000004, 000009),
(2000005, 000010),
(1000005, 000011),
(1000006, 000012),
(1000007, 000013),
(1000008, 000014);


INSERT INTO "HOME" ("id", "type", "build_year", "bedrooms", "bathrooms", "description") VALUES
(000000, "ΔΙΑΜΕΡΙΣΜΑ", 2004, 3, 2, "1 Σαλόνι, 1 Κουζίνα, Τύπος δαπέδων: Πλακάκι, Κλιματισμός: Ναι, Σοφίτα: Οχι, Τζάκι: Οχι, Playroom: Οχι, Πόρτα ασφαλείας: Ναι, Ασανσέρ: Ναι, Επιπλωμένο: Οχι, Εσωτερική σκάλα: Οχι, Κουφώματα: Αλουμινίου, Διπλά τζάμια: Ναι, Σίτες: Ναι, Βαμμένο: Ναι, Ενδοδαπέδια θέρμανση: Οχι"),
(000001, "ΔΙΑΜΕΡΙΣΜΑ", 2021, 2, 1, "1 Σαλόνι, 1 Κουζίνα, Τύπος δαπέδων: Πλακάκι, Κλιματισμός: Οχι, Σοφίτα: Οχι, Τζάκι: Ναι, Playroom: Οχι, Πόρτα ασφαλείας: Ναι, Ασανσέρ: Ναι, Επιπλωμένο: Οχι, Εσωτερική σκάλα: Οχι, Κουφώματα: Αλουμινίου, Διπλά τζάμια: Ναι, Σίτες: Ναι, Βαμμένο: Ναι, Ενδοδαπέδια θέρμανση: Οχι"),
(000002, "ΒΙΛΑ", 2005, 7, 4, "Κλιματισμός: Οχι, Σοφίτα: Ναι, Τζάκι: Οχι, Playroom: Ναι, Πόρτα ασφαλείας: Οχι, Ασανσέρ: Οχι, Επιπλωμένο: Ναι, Εσωτερική σκάλα: Ναι, Διπλά τζάμια: Ναι, Σίτες: Οχι, Βαμμένο: Οχι, Ενδοδαπέδια θέρμανση: Οχι"),
(000003, "ΓΚΑΡΣΟΝΙΕΡΑ", 1974, 1, 1, "1 Κουζίνα, Τύπος δαπέδων: Πλακάκι, Κλιματισμός: Ναι, Σοφίτα: Οχι, Τζάκι: Οχι, Playroom: Οχι, Πόρτα ασφαλείας: Οχι, Ασανσέρ: Ναι, Επιπλωμένο: Οχι, Εσωτερική σκάλα: Οχι, Κουφώματα: Ξύλινα, Διπλά τζάμια: Οχι, Σίτες: Οχι, Βαμμένο: Ναι, Ενδοδαπέδια θέρμανση: Οχι"),
(000007, "ΔΙΑΜΕΡΙΣΜΑ", 1979, 2, 1, "1 Σαλόνι, 1 Κουζίνα, Τύπος δαπέδων: Πλακάκι, Κλιματισμός: Ναι, Σοφίτα: Οχι, Τζάκι: Ναι, Playroom: Οχι, Πόρτα ασφαλείας: Οχι, Ασανσέρ: Οχι, Επιπλωμένο: Οχι, Εσωτερική σκάλα: Οχι, Κουφώματα: Αλουμινίου, Διπλά τζάμια: Ναι, Σίτες: Οχι, Βαμμένο: Οχι, Ενδοδαπέδια θέρμανση: Οχι"),
(000009, "ΜΟΝΟΚΑΤΟΙΚΙΑ", 2004, 5, 1, "1 Σαλόνι, 1 Κουζίνα, 2 WC, Τύπος δαπέδων: Πλακάκι, Κλιματισμός: Ναι, Σοφίτα: Οχι, Τζάκι: Ναι, Playroom: Οχι, Πόρτα ασφαλείας: Ναι, Ασανσέρ: Οχι, Επιπλωμένο: Ναι, Εσωτερική σκάλα: Ναι, Κουφώματα: Αλουμινίου, Διπλά τζάμια: Ναι, Σίτες: Οχι, Βαμμένο: Ναι, Ενδοδαπέδια θέρμανση: Οχι"),
(000014, "ΔΙΑΜΕΡΙΣΜΑ", 2007, 3, 2, "1 Σαλόνι, 1 Κουζίνα, Τύπος δαπέδων: Πλακάκι, Κλιματισμός: Οχι, Σοφίτα: Οχι, Τζάκι: Ναι, Playroom: Οχι, Πόρτα ασφαλείας: Ναι, Ασανσέρ: Ναι, Επιπλωμένο: Οχι, Εσωτερική σκάλα: Οχι, Κουφώματα: Αλουμινίου, Διπλά τζάμια: Ναι, Σίτες: Ναι, Βαμμένο: Ναι, Ενδοδαπέδια θέρμανση: Οχι");



INSERT INTO "BUSINESS" ("id", "type", "build_year", "description") VALUES
(000006, "ΓΡΑΦΕΙΟ", 2000, "1 WC, Τύπος δαπέδων: Πλακάκι, Κλιματισμός: Ναι, Ψευδοροφή: Οχι, Δομημένη καλωδίωση: Οχι, Εσωτερική σκάλα: Οχι, Με εξοπλισμό: Οχι, Βαμμένο: Οχι, Ενδοδαπέδια θέρμανση: Οχι"),
(000008, "ΚΑΤΑΣΤΗΜΑ", 1971, "1 WC, Κλιματισμός: Οχι, Ψευδοροφή: Οχι, Δομημένη καλωδίωση: Οχι, Εσωτερική σκάλα: Οχι, Με εξοπλισμό: Οχι, Ρεύμα: Τριφασικό, Βαμμένο: Οχι, Ενδοδαπέδια θέρμανση: Οχι"),
(000010, "ΓΡΑΦΕΙΟ", 1991, "Τύπος δαπέδων: Πλακάκι, Κλιματισμός: Οχι, Ψευδοροφή: Οχι, Δομημένη καλωδίωση: Οχι, Εσωτερική σκάλα: Οχι, Με εξοπλισμό: Οχι, Βαμμένο: Οχι, Ενδοδαπέδια θέρμανση: Οχι"),
(000012, "ΓΡΑΦΕΙΟ", 1975, "1 WC, Τύπος δαπέδων: Μωσαϊκό, Κλιματισμός: Οχι, Ψευδοροφή: Οχι, Δομημένη καλωδίωση: Οχι, Εσωτερική σκάλα: Οχι, Με εξοπλισμό: Οχι, Ρεύμα: Μονοφασικό, Βαμμένο: Οχι, Ενδοδαπέδια θέρμανση: Οχι"),
(000013, "ΞΕΝΟΔΟΧΕΙΟ", 1994, "Κλιματισμός: Οχι, Ψευδοροφή: Οχι, Δομημένη καλωδίωση: Οχι, Εσωτερική σκάλα: Οχι, Με εξοπλισμό: Οχι, Ρεύμα: Τριφασικό, Βαμμένο: Οχι, Ενδοδαπέδια θέρμανση: Οχι");



INSERT INTO "LAND" ("id", "description") VALUES
(000004, "Εντός σχεδίου πόλεως: Οχι, Πρόσοψης: Ναι, Γωνιακό: Οχι, Είδος δρόμου: Άσφαλτος, Επενδυτικό: Οχι, Κατάλληλο για αγροτική χρήση: Οχι, Απόσταση απο τη θάλασσα (m): 3000 μέτρα"),
(000005, "Εντός σχεδίου πόλεως: Ναι, Πρόσοψης: Οχι, Γωνιακό: Οχι, Οικιστική ζώνη, Είδος δρόμου: Άσφαλτος, Επενδυτικό: Οχι, Απόσταση απο τη θάλασσα (m): 600 μέτρα"),
(000011, "Εντός σχεδίου πόλεως: Ναι, Πρόσοψης: Ναι, Μήκος πρόσοψης: 17 μέτρα, Γωνιακό: Οχι, Οικιστική ζώνη, Είδος δρόμου: Άσφαλτος, Επενδυτικό: Ναι, Κατάλληλο για αγροτική χρήση: Ναι, Απόσταση απο τη θάλασσα (m): 3000 μέτρα");



INSERT INTO "SALE" ("sale_id", "price", "tm", "date") VALUES
(1000000, 125000,	104,	NULL),
(1000001, 103000,	81,		NULL),
(1000002, 117000,	467,	"2021-03-23"),
(1000003, 35000,	139,	NULL),
(1000004, 230000,	275,	"2021-11-13"),
(1000005, 32000,	200,	NULL),
(1000006, 100000,	60,		"2021-02-05"),
(1000007, 10000000,	4600,	"2019-09-13"),
(1000008, 45000,	130,	NULL);



INSERT INTO "RENTAL" ("rental_id", "price_month", "tm", "warranty", "startdate", "enddate") VALUES
(2000000, 5000,		550,	NULL, NULL, NULL),
(2000001, 380,		32,		NULL, NULL, NULL),
(2000002, 500,		62,		2, "2021-04-12", "2024-04-12"),
(2000003, 730,		90,		1, "2020-01-01", "2025-01-01"),
(2000004, 800,		280,	NULL, NULL, NULL),
(2000005, 2000,	    350,	NULL, NULL, NULL);





COMMIT;
