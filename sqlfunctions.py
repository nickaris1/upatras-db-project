import sqlite3

def convert_to_type(sqltype, data : str): # convert sql type to python type
    if len(data) == 0:
        return data

    if (sqltype == "integer"):
        return int(data)
    elif(sqltype.lower() == "bool" or sqltype.lower() == "boolean"):
        if data.lower() == "false":
            return False
        elif data.lower() == "true":
            return True
        else:
            return bool(data)
    else:
        return str(data)

def create_entry(mydb, cursor, tablename, ls):
    try:
        table_column_names = get_table_column_names(cursor, tablename)
        table_column_types = get_table_column_types(cursor, tablename)
        if (len(table_column_names) != len(ls)): # if number of values is different of the number of values the table gets STOP
            return False
        print(table_column_types)
        data = [x for x in ls if len(x) != 0] # Clean Data (only strings with length > 0 will be accepted) in order to have NULL
        ctype = []
        cname = []
        for i in range(len(table_column_types)):
            if (len(ls[i]) != 0):
                ctype.append(table_column_types[i])
                cname.append(table_column_names[i])

        sql_command = f"INSERT INTO {tablename} ({str(cname)[1:-1]}) VALUES (" # Create query

        for i in range(len(data)):
            sql_command += f"'{convert_to_type(ctype[i], data[i])}'"
            if i < len(data) - 1:
                sql_command += ", "

        sql_command += ")" 
        print(sql_command)
        cursor.execute(sql_command)
        mydb.commit()
        return True
    except Exception as e:
        print(f"error: {e}")
        return False

def update_entry(mydb, cursor, tablename, ls, pk, id):
    try:
        table_column_names = get_table_column_names(cursor, tablename)
        table_column_types = get_table_column_types(cursor, tablename)
        if (len(table_column_names) != len(ls)):
            return False
        print(table_column_types)
        data = [x for x in ls if len(x) != 0] # if number of values is different of the number of values the table gets STOP
        ctype = []
        cname = []
        for i in range(len(table_column_types)):  # Clean Data (only strings with length > 0 will be accepted) in order to have NULL
            if (len(ls[i]) != 0):
                ctype.append(table_column_types[i])
                cname.append(table_column_names[i])
        sql_command = f"UPDATE {tablename} SET " # Create Query

        for i in range(len(data)):
            sql_command += f"'{cname[i]}'='{convert_to_type(ctype[i], data[i])}'"
            if i < len(data) - 1:
                sql_command += ", "

        sql_command += f'WHERE "{pk}"={id};'
        print(sql_command)
        cursor.execute(sql_command)
        mydb.commit()
        return True
    except Exception as e:
        print(f"error: {e}")
        return False

def get_table(cursor, tablename):   # Get all values from table
    cursor.execute("select * from " + tablename)
    return cursor.fetchall()

def get_table_id(cursor, tablename, primary_key, id):   # Get Table entry with key=id
    print(f"select * from {tablename} WHERE {primary_key}=={id};")
    cursor.execute(f"select * from {tablename} WHERE {primary_key}=={id};")
    return cursor.fetchall()

def get_table_column_names(cursor, tablename):  # Get Column names
    cursor.execute("pragma table_info('" + tablename + "')") # pragma does not return GENERATED VALUES
    d = [column[1] for column in cursor.fetchall()]
    c = cursor.execute(f'select * from "{tablename}"')

    names = [description[0] for description in c.description]
    if len(names) > len(d):
        return names
    return d

def get_table_column_types(cursor, tablename):  # Get Column Types
    cursor.execute("pragma table_info('" + tablename + "')")
    return [column[2] for column in cursor.fetchall()]

def get_table_info(cursor, tablename):  # Get Table Info In dictionary
    cursor.execute("pragma table_info('" + tablename + "')")
    d = {}
    for column in cursor.fetchall():
        d[column[1]] = {}
        d[column[1]]["cid"]         = column[0]
        d[column[1]]["name"]        = column[1]
        d[column[1]]["type"]        = column[2]
        d[column[1]]["notnull"]     = column[3]
        d[column[1]]["default"]     = column[4]
        d[column[1]]["pk"]          = column[5]
    
    return d

def get_primary_key(cursor, tablename): # Get List Primary Keys of table
    tableinfo = get_table_info(cursor, tablename)
    primary_keys = []
    for i in tableinfo:
        if(tableinfo[i]["pk"] == 1):
            primary_keys.append(i)
    return primary_keys

def get_table_foreign_keys(cursor, tablename):  # Get List Foreign Keys of table
    cursor.execute("pragma foreign_key_list('" + tablename + "')")
    d = {}
    for column in cursor.fetchall():
        d[column[3]] = create_table_fk(column)
    return d

def create_table_fk(column):
    l = {}
    l["id"]         = column[0]
    l["seq"]        = column[1]
    l["table"]      = column[2]
    l["from"]       = column[3]
    l["to"]         = column[4]
    l["on_update"]  = column[5]
    l["on_delete"]  = column[6]
    l["match"]      = column[7]
    
    return l

def execute_query(mydb, cursor, query): # Run Custom Query
    cursor.execute(query)
    mydb.commit()

def deleteItems(mydb, cursor, tablename, ID, item): # Delete Item
    try:
        sql_command = "DELETE FROM " + tablename + " WHERE " + str(ID) + " = '" + str(item) + "'"
        cursor.execute(sql_command)
        mydb.commit()
        return True
    except Exception as e:
        print(e)
        return False
